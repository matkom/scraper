using System.Net;
using Microsoft.Extensions.Logging;
using Scraper.Interfaces;
using Scraper.Models;

namespace Scraper.Repositories
{
    public class BdoCouponsWebRepository : ICouponsRepository
    {
        private const string GARMOTH_API_URL = "https://garmoth.com/api/coupons";
        private const string USER_AGENT_KEY = "User-Agent";
        private const string USER_AGENT_VALUE = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36";

        private readonly ILogger _logger;
        private readonly HttpClient _httpClient;
        private readonly IJsonSerializer _jsonSerializer;

        public BdoCouponsWebRepository(ILogger<BdoCouponsWebRepository> logger, HttpClient httpClient,
                                       IJsonSerializer jsonSerializer)
        {
            _logger = logger;
            _httpClient = httpClient;
            _jsonSerializer = jsonSerializer;
        }

        public async Task<List<BdoCoupon>> GetCouponsAsync()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, GARMOTH_API_URL);
            request.Headers.Add(USER_AGENT_KEY, USER_AGENT_VALUE);

            try
            {
                _logger.LogDebug($"Sending HTTP GET request to {GARMOTH_API_URL}");
                var response = await _httpClient.SendAsync(request);
                var responseTxt = await response.Content.ReadAsStringAsync();
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    _logger.LogError($"Did not get any coupons from {GARMOTH_API_URL}. StatusCode: {(int)response.StatusCode}");
                    _logger.LogError(responseTxt);
                    return new List<BdoCoupon>();
                }
                return _jsonSerializer.Deserialize<List<BdoCoupon>>(responseTxt) ?? new List<BdoCoupon>();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unexpected exception while requesting coupons from {GARMOTH_API_URL}");
                return new List<BdoCoupon>();
            }
        }
    }
}
