using Scraper.Models;

namespace Scraper.Interfaces
{
    public interface ICouponsRepository
    {
        public Task<List<BdoCoupon>> GetCouponsAsync();
    }
}
