namespace Scraper.Interfaces
{
    public interface ICouponsService
    {
        Task<string> GetCouponsResponseAsync();
    }
}
