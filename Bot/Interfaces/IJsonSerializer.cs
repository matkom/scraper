namespace Scraper.Interfaces
{
    public interface IJsonSerializer
    {
        T Deserialize<T>(string json);
    }
}
