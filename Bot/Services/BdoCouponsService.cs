using System.Text;
using Scraper.Interfaces;
using Scraper.Helpers;

namespace Scraper.Services
{
    public class BdoCouponsService : ICouponsService
    {
        private readonly ICouponsRepository _couponsRepository;

        public BdoCouponsService(ICouponsRepository couponsRepository)
        {
            _couponsRepository = couponsRepository;
        }

        public async Task<string> GetCouponsResponseAsync()
        {
            var coupons = await _couponsRepository.GetCouponsAsync();
            if (coupons == null || coupons.Count == 0)
            {
                return "Could not get any coupons at the moment. Sorry :(";
            }

            var now = DateTime.UtcNow;
            var strBuilder = new StringBuilder();
            foreach (var coupon in coupons.Where((x) => x.ExpiresAt > now))
            {
                strBuilder.AppendLine(coupon.Code);
                strBuilder.AppendLine($"Created: {DateTimeHelper.GetTimeString(now, coupon.CreatedAt)} ago");
                strBuilder.AppendLine($"Expires in: {DateTimeHelper.GetTimeString(coupon.ExpiresAt, now)}");
                strBuilder.AppendLine();
            }

            return strBuilder.ToString();
        }
    }
}
