using System.Reflection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Discord;
using Discord.WebSocket;
using Discord.Interactions;
using Scraper.Helpers;

namespace Scraper.Services
{
    public class BotService : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _config;
        private readonly IServiceProvider _serviceProvider;
        private readonly DiscordSocketClient _discordClient;
        private readonly InteractionService _interactionService;

        public BotService(ILogger<BotService> logger, IConfiguration config, IServiceProvider serviceProvider,
                          DiscordSocketClient discordClient, InteractionService interactionService)
        {
            _config = config;
            _logger = logger;
            _serviceProvider = serviceProvider;
            _discordClient = discordClient;
            _interactionService = interactionService;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                _logger.LogInformation("Loading interaction modules");
                await _interactionService.AddModulesAsync(Assembly.GetEntryAssembly(), _serviceProvider);

                _discordClient.Log += (msg) => LogHelper.DiscordLog(_logger, msg);
                _interactionService.Log += (msg) => LogHelper.DiscordLog(_logger, msg);

                _discordClient.InteractionCreated += HandleInteraction;
                _discordClient.Ready += SetupCommands;

                _logger.LogInformation("Connecting to discord");
                await _discordClient.LoginAsync(TokenType.Bot, _config["SCRAPER_TOKEN"]);
                await _discordClient.StartAsync();
                await Task.Delay(-1, cancellationToken);
            }
        }

        private async Task HandleInteraction(SocketInteraction arg)
        {
            try
            {
                var context = new SocketInteractionContext(_discordClient, arg);
                await _interactionService.ExecuteCommandAsync(context, _serviceProvider);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unexpected error occured when executing command");
                if (arg.Type == Discord.InteractionType.ApplicationCommand)
                {
                    _logger.LogDebug("Deleting response to hide a failure");
                    await arg.GetOriginalResponseAsync()
                        .ContinueWith(async (msg) => await msg.Result.DeleteAsync());
                }
            }
        }

        private async Task SetupCommands()
        {
            if (ConfigHelper.IsDebug())
            {
                if (!UInt64.TryParse(_config["SCRAPER_GUILD"], out var guildId))
                {
                    _logger.LogCritical("Configured guild id is not a valid uint64. Cannot register guild commands");
                    return;
                }
                await _interactionService.RegisterCommandsToGuildAsync(guildId, true);
            }
            else
            {
                await _interactionService.RegisterCommandsGloballyAsync(true);
            }
        }
    }
}
