using System.Text.Json.Serialization;

namespace Scraper.Models
{
    public class BdoCoupon
    {
        [JsonPropertyName("code")]
        public string? Code { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonPropertyName("expire_at")]
        public DateTime ExpiresAt { get; set; }
    }
}
