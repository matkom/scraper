﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Discord.Interactions;
using Discord.WebSocket;
using Scraper.Interfaces;
using Scraper.Services;
using Scraper.Repositories;
using Scraper.Helpers;

namespace Scraper
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = Host.CreateDefaultBuilder(args)
                .UseSystemd()
                .UseWindowsService()
                .ConfigureServices(services =>
                {
                    services
                        .AddSingleton<DiscordSocketClient>()
                        .AddSingleton<InteractionService>()
                        .AddSingleton<HttpClient>()
                        .AddTransient<IJsonSerializer, JsonSerializerWrapper>()
                        .AddTransient<ICouponsRepository, BdoCouponsWebRepository>()
                        .AddTransient<ICouponsService, BdoCouponsService>()
                        .AddHostedService<BotService>();
                })
                .Build();

            await host.RunAsync();
        }
    }
}
