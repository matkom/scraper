using Microsoft.Extensions.Logging;
using Discord.Interactions;
using Scraper.Interfaces;

namespace Scraper.Modules
{
    public class BdoModule : InteractionModuleBase<SocketInteractionContext>
    {
        private readonly ILogger _logger;
        private readonly ICouponsService _couponsService;

        public BdoModule(ILogger<BdoModule> logger, ICouponsService couponsService)
        {
            _logger = logger;
            _couponsService = couponsService;
        }

        [SlashCommand("bdo-coupons", "Get black desert online coupons")]
        public async Task BdoCoupons()
        {
            _logger.LogDebug("bdo-coupons command invoked");
            var coupons = await _couponsService.GetCouponsResponseAsync();
            await RespondAsync(coupons);
        }
    }
}
