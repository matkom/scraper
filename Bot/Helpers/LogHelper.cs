using Microsoft.Extensions.Logging;
using Discord;

namespace Scraper.Helpers
{
    public static class LogHelper
    {
        public static Task DiscordLog(ILogger logger, LogMessage msg)
        {
            switch (msg.Severity)
            {
                case LogSeverity.Critical:
                    logger.LogCritical(msg.ToString());
                    break;

                case LogSeverity.Error:
                    logger.LogError(msg.Exception, msg.ToString());
                    break;

                case LogSeverity.Verbose:
                case LogSeverity.Debug:
                    logger.LogDebug(msg.ToString());
                    break;

                case LogSeverity.Warning:
                    logger.LogWarning(msg.Exception.Message);
                    break;

                case LogSeverity.Info:
                    logger.LogInformation(msg.ToString());
                    break;
            }

            return Task.CompletedTask;
        }
    }
}
