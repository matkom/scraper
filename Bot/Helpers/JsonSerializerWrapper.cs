using System.Text.Json;
using Scraper.Interfaces;

namespace Scraper.Helpers
{
    public class JsonSerializerWrapper : IJsonSerializer
    {
        public T Deserialize<T>(string json)
        {
            return JsonSerializer.Deserialize<T>(json) ?? default!;
        }
    }
}
