namespace Scraper.Helpers
{
    public static class DateTimeHelper
    {
        const int SECOND = 1;
        const int MINUTE = 60 * SECOND;
        const int HOUR = 60 * MINUTE;
        const int DAY = 24 * HOUR;
        const int MONTH = 30 * DAY;
        const int YEAR = 12 * MONTH;

        public static string GetTimeString(DateTime since, DateTime until)
        {
            var timeSpan = since - until;
            var delta = Math.Abs(timeSpan.TotalSeconds);

            if (delta <= MINUTE)
                return timeSpan.Seconds == 1 ? "one second" : $"{timeSpan.Seconds} seconds";

            if (delta < 2 * MINUTE)
                return "a minute";

            if (delta < 45 * MINUTE)
                return $"{timeSpan.Minutes} minutes";

            if (delta < 90 * MINUTE)
                return "an hour ago";

            if (delta < DAY)
                return $"{timeSpan.Hours} hours";

            if (delta < 2 * DAY)
                return "yesterday";

            if (delta < MONTH)
                return $"{timeSpan.Days} days";

            if (delta < YEAR)
            {
                var months = Convert.ToInt32(Math.Floor((double)timeSpan.Days / 30));
                return months <= 1 ? "one month" : $"{months} months";
            }

            var years = Convert.ToInt32(Math.Floor((double)timeSpan.Days / 365));
            return years <= 1 ? "one year" : $"{years} years";
        }
    }
}
